import React, {useState, useRef, useEffect} from 'react';
import Rezepte from './Rezepte';
import WarenKorb from './Warenkorb';
import Axios from 'axios';


function App() {

  //testDATA
  //{id:1, name:"kartoffelgratin",selected:false,menge:1},{id:2, name:"Milchreis",selected:false,menge:1},{id:3, name:"Kürbissuppe",selected:false,menge:1},{id:4, name:"Hachkbällchen",selected:false,menge:1},{id:5, name:"McNuggets",selected:false,menge:1}
  const [rezepte, setRezepte ] = useState([])
  const [warenkorb, setWarenkorb] = useState([])
  const rezeptRef = useRef()
  const LOCAL_STORAGE_KEY = "warenkorb.waren"
  //console.log(warenkorb);
  
  // AXIOS REQUEST TO GET THE RECIPE DATA
  function sendRequest(){
    
  Axios.get('/api')
  .then(function(res){
    setRezepte([...rezepte,...res.data.data]);
    console.log(res);
  })
  .catch(function(error){
    console.log(error);
  })

  }


  function handleSearchRecipe(e){
    const name = rezeptRef.current.value
    if(name === '') return 
    rezeptRef.current.value = null;
  }

    useEffect(()=>{
      console.log(rezepte);
      if(rezepte.length === 0){
        sendRequest();
      }
      
    },[])
  
  
  /*useEffect(()=>{
    const storedWaren = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if(storedWaren) setWarenkorb(storedWaren)
  },[])

  useEffect(()=>{
    localStorage.setItem(LOCAL_STORAGE_KEY,JSON.stringify(warenkorb))
  },[warenkorb])
  */

  function deleteWareFromWarenkorb(warenId){
    let array = [...warenkorb];
    let index = warenkorb.findIndex(x => x.id === warenId)
    console.log(array[index]);
    let removed = array.splice(index,1);
    console.log(array);
    //console.log(...array);
    setWarenkorb(array);

    //console.log(warenkorb[index]);
  }

  function addRecipeToWarenkorb(rezeptId){
    let index = rezepte.findIndex(y=> y.id === rezeptId);
    if(warenkorb.find(x => x.id === rezeptId)===undefined){
      setWarenkorb((prevWarenkorb)=>{
        return [...prevWarenkorb,rezepte[index]];
      })
    }else{
      warenkorb.find(y => y.id === rezeptId)
    }

  }


  function handleWarenkorb(e){
    setWarenkorb((prevWarenkorb) =>{
      return [...prevWarenkorb];
    })
  }

  return (
    <div className="container">
      <div className="banneri">
        <div className="items">
          <h1 className="logo">Kraut und Rüben</h1>
        </div>
      </div>

      <nav className="navbar navbar-light bg-light justify-content-center">
  <div className="container-fluid">
    <a className="navbar-brand" href="#">Default</a>
    <a className="navbar-brand" href="#">Default</a>
    <a className="navbar-brand" href="#">Default</a>
  </div>
</nav>
      <div className="container">
      
      <div className="row">
          <div className="col-8">
           
            <Rezepte rezepte={rezepte} setRezepte={setRezepte} addRecipe={addRecipeToWarenkorb}/>
            </div>

          <div className="col">
            <span className="justify-content-center"><b>Warenkorb</b></span>
            <ul>
              <WarenKorb  warenkorb={warenkorb} setWarenkorb={setWarenkorb} delWare={deleteWareFromWarenkorb}  />
              </ul>
          </div>
          </div>
     
      </div>
   
      
  




      

    </div>
  );
}

export default App;
