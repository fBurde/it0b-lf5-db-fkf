import React from 'react'

export default function Warenkorb({warenkorb,setWarenkorb,delWare}) {

    //Mapping Dynamisch Ware zum Warenkorb
    return (
        warenkorb.map(ware =>{
            return <Ware key={ware.id}  ware={ware} delWare={delWare}  />
        })
    )
}


const Ware = (props) =>{
    return (
        <div className="list-group justify-content-end">
            <a className="list-group-item list-group-item-action list-group-item-light">
 
                <img className="img-thumbnail float-left" width="100px" height="80px" src="https://www.daskochrezept.de/sites/default/files/styles/169_xl/public/2018-05/00857204schnelleskartoffelgratinmitsahneundkaese.jpg?h=32751ef5&itok=7kGMxElb"></img>
            

            <div className="container">
            Name:<span className="badge badge-primary" ><h4>{props.ware.name}</h4></span>
            Preis:<span className="badge badge-primary" ><h4>{props.ware.preis * props.ware.menge}</h4></span>
            Menge:<span className="badge badge-primary" ><h4>{props.ware.menge}</h4></span>
            <button className="btn btn-danger" onClick={() => props.delWare(props.ware.id)}>x</button>
            </div>
            </a>

       
        </div>
    )
}
