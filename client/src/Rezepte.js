import React from 'react'
import {Card} from 'bootstrap'

export default function Rezepte({rezepte,addRecipe}) {
// Wrapper für Rezept Komponenten
        
    return (

        //mapping von dynamisch generierten Rezept Komponenten ans DOM  
        <div className="row">
        {rezepte.map(rezept =>{
            return <Rezept key={rezept.REZEPTID}  rezept={rezept} addRecipe={addRecipe}/>
        })}
        </div>
    )
}



const Rezept = (props) =>{


    function addItemAmount(){

    }


    //Card Item Wrapper für Rezepte/Zutaten die aus der Datenbank kommen
    return (
        <div className="col-3">
        <div className="card">
  <img src="https://cdn.gutekueche.de/upload/rezept/937/kartoffelgratin-ganz-schnell.jpg" className="card-img-top" alt="bild von Essen" />
  <div className="card-body">
    <h5 className="card-title">{props.rezept.NAME}</h5>
    <p className="card-text">{props.rezept.NÄHRWERT +"Kalorien"}
    <br/>
    <p>{ props.rezept.PREIS + "€"}</p>
    </p>
    <div className="d-flex justify-content-center "><a className="btn btn-secondary">-</a><h4 className="mx-2">{props.rezept.menge}</h4><a className="btn btn-secondary" onClick="">+</a></div>
    <a className="btn btn-primary d-flex my-1 justify-content-center" onClick={() => props.addRecipe(props.rezept.id)}>hinzufügen</a>
  </div>
        </div>
        </div>
    )
}