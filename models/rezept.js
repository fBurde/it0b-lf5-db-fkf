const {Sequelize, DataTypes} = require('sequelize');
const db = require('../database');

const Rezept = db.define('rezepte',{
    NAME:{
        type: DataTypes.STRING,
        allowNull: false
    },
    PREIS:{
        type: DataTypes.FLOAT,
        allowNull: false,
        unique: true
    },
    NÄHRWERT:{
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
    PROTEIN:{
        type: DataTypes.FLOAT,
        allowNull: false
    },
    
    KOHLENHYDRATE:{
        type: DataTypes.FLOAT,
        allowNull: false
    },
    KATEGORIE:{
        type: DataTypes.STRING,
        allowNull: false
    }

})

module.exports = Rezept;